package com.example.demo;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import com.mysema.commons.lang.Assert;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UploadController.class, secure = false)
public class UploadControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private UploadController uploadController;	
	
	@Before
	public void setUp() throws Exception {
		uploadController=new UploadController();
	}

	@After
	public void tearDown() throws Exception {
		uploadController=null;
	}

	@org.junit.Test
	public void uploadFile() throws Exception {
		
		String successMessage="";
		try{
			 successMessage=uploadController.singleFileUpload(readFile());
		}
		catch (Exception e) {
            e.printStackTrace();
        }
		assertEquals("You successfully uploaded  filename.txt",successMessage);
	}
	
	public static File readFile(){
		File file =null;
		try{
			 file =new File("C:\\Users\\pankaj\\Desktop\\filename.txt");
		}
		catch( FileNotFoundException e){
			e.printStackTrace();
		}
	
		return file;
	}

}
